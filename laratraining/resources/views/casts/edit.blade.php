@extends('layouts.master')

@section('title')
Edit Cast : {{$casts->nama}}
@endsection

@section('content')

<form action="/cast/{{$casts->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="inputNama">Nama</label>
        <input type="text" class="form-control" value="{{$casts->nama}}" name="nama" id="nama" placeholder="Masukkan Nama">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Umur</label>
        <input type="number" class="form-control" value="{{$casts->umur}}" name="umur" id="umur" placeholder="Masukkan Umur">
        @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="title">Bio</label>
        <textarea class="form-control" name="bio" id="bio" cols="30" rows="10" placeholder="Masukkan Bio">{{$casts->bio}}</textarea>
        @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection