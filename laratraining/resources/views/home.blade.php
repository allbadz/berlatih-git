@extends('layouts.master')

@section('title')
Main Page
@endsection

@section('content')

<body>
    <h1>SanberBook</h1>
    <h2>Social Media Developer Santai Berkualitas</h2>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
    <h3>Benefit Join di Sanberbook</h3>
    <ul>
        <li>
            <p>Mendapatkan motivasi dari sesama developer</p>
        </li>
        <li>
            <p>Sharing knowledge dari para mastah Sanber</p>
        </li>
        <li>
            <p>Dibuat oleh calon web developer terbaik</p>
        </li>
    </ul>
    <h3>Cara Bergabung ke Sanberbook</h3>
    <ol>
        <li>
            <p>Mengunjungi Website ini</p>
        </li>
        <li>
            <p>Mendaftar di <a href="/register">Form Sign Up</a></p>
        </li>
        <li>
            <p>Selesai!</p>
        </li>
    </ol>
</body>

@endsection